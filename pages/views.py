from django.views.generic import TemplateView


class HomePageView(TemplateView):
    template_name = "home.html"


class GardenPageView(TemplateView):
    template_name = "garden.html"


class IdPageView(TemplateView):
    template_name = "id.html"


class ArticlePageView(TemplateView):
    template_name = "article_list.html"
