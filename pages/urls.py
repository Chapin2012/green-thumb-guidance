from django.urls import path

from .views import HomePageView
from .views import GardenPageView
from .views import IdPageView
from .views import ArticlePageView


urlpatterns = [
    path("", HomePageView.as_view(), name="home"),
    path("garden", GardenPageView.as_view(), name="garden"),
    path("id", IdPageView.as_view(), name="id"),
    path("article", ArticlePageView.as_view(), name="article"),
]
